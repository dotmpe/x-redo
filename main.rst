x-redo
======
:created: 2016-08-13


Intro
------
Looking for ideas on configuration of complex builds. Autoconf, automake only
leads to C-based development naturally, but with lots of boilerplate that
obfuscate the core and makes assumptions about the projects lifecycle.
Other build systems are also too complex (SCons) or ugly (Ant) or too
language/library specific (Grunt).

Instead want something more simple for building, and a effective manner of
parametrization. And both suitable for any software or documentation project.


For building, taking look at reimplementations of Make.
So found redo mentioned__. Pretty sweet and simple, but needs a bit of thought
to get head around parts.

This contains the examples from the introduction of the ``apenwarr/redo`` ReadMe__.
And a Vagrant file to setup a VirtualBox VM with redo installed, for running
testing at GNU/Linux.

There is also a haskell__ implementation.


Conclusion
----------
Redo files are terse and kept close with the file it generates.
This leaves plenty of room for project specific setup, while effeciently
handling the build part.

Redo runs purely to generate file targets -- the only status output is has
is wether targets are successfully build or errored. If up to date, just the
requested target is printed. Besides `all` and `default` there seem to be no
special or `virtual` (``PHONY``) targets.

Its installation does not seem very clean -- there's no dist. or package.
But when installed, it provides a nice and clean substitute for both Makefile
and Sh scripts in a project.


Raw refs
--------
http://cr.yp.to/redo.html
  http://cr.yp.to/redo/atomic.html
    Describes briefly what redo does vs. traditional make.

  Also has an "honest" appraisal about prerequisites and the problem
  of trying to list all of these.

http://thedjbway.b0llix.net/future.html
  Brief intro and key points, links on `jdb` -- Dan J. Bernstein. Who
  wrote ``redo`` as small build part of ``djbdns``, or ``daemontools``?


.. __: GNU_Make_Alternatives_
.. _GNU_Make_Alternatives: https://news.ycombinator.com/item?id=4249369

.. __: https://github.com/apenwarr/redo/

.. __: https://github.com/jekor/redo


